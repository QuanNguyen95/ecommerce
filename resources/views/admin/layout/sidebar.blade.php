<aside>
    <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu">                
        <li class="active">
        <a class="" href="{{ route('index_admin') }}">
            <i class="icon_house_alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" class="">
            <i class="icon_document_alt"></i>
            <span>Categories</span>
            <span class="menu-arrow arrow_carrot-right"></span>
        </a>
        <ul class="sub">
            <li><a class="" href="{{ route('category_add') }}">Add Category</a></li>                          
          <li><a class="" href="{{ route('category_list') }}">View Categories</a></li>
        </ul>
</li>       
<li class="sub-menu">
    <a href="javascript:;" class="">
        <i class="icon_lifesaver"></i>
        <span>Products</span>
        <span class="menu-arrow arrow_carrot-right"></span>
    </a>
    <ul class="sub">
        <li><a class="" href="{{ route('product_add') }}">Add Products</a></li>
        <li><a class="" href="{{ route('product_list') }}">View Products</a></li>
    </ul>
</li>
<li class="sub-menu">
    <a class="" href="{{ route('users_list') }}">
        <i class="icon_contacts_alt"></i>
        <span>User</span>
    </a>
</li>
<li>
    <a class="" href="">
        <i class="icon_genius"></i>
        <span>Order</span>
    </a>
</li>

</ul>
<!-- sidebar menu end-->
</div>
</aside>