@extends('admin.layout.master')

@section('content')

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-6">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title">Add New Product</div>
                    </div>
                    <div class="panel-body">

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $err)
                                <li>{{ $err }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @if (session('message'))
                        <div class="alert alert-success col-lg-12" id="message">
                            {{session('message')}}
                        </div>
                        @endif

                        <form action="{{ route('product_add') }}" method="post" accept-charset="utf-8" enctype='multipart/form-data'>
                            Category: 
                            <select class="form-control" name="cat_id">
                                @foreach($category as $cate)
                                <option value="{{ $cate->id }}" @if (old('cat_id') == $cate->id)
                                    selected="selected"
                                @endif>{{ $cate->name }}</option>
                                @endforeach
                            </select>
                            <br>

                            Name:    <input type="text" name="pro_name" class="form-control" value="{{ old('pro_name') }}" required="required" />
                            <br/>
                            Price     <input type="number" name="pro_price" class="form-control" value="{{ old('pro_price',0) }}" required="required" step="0.01" min="0" />
                            <br/>

                            Code:    <input type="text" name="pro_code" class="form-control" value="{{ old('pro_code') }}" required="required"/>
                            <br/>
                            Imgage:     <input type="file" name="pro_img" class="form-control">
                            <br/>

                            Details:    <textarea name="pro_info" class="form-control ckeditor" rows="5" required="required">{{ old('pro_info') }}</textarea>
                            <br/>

                            <input type="radio" name="sale" value="1" id="onSale" @if (old('sale') == 1)
                                checked="checked"
                            @endif
                                
                           > Yes 
                            <input type="radio" name="sale" value="0" id="noSale" @if (old('sale') == 0 OR old('sale') == NULL)
                                checked="checked"
                            @endif> No 
                            <br>
                            <div id="amountDiv">
                                Sale  Price:     <input id="sql_price" type="number" name="sql_price" class="form-control" value="0" step="0.01" min="0">
                                <br/>
                            </div>
                            
                            <br>

                            Alt Image: <input type="file" name="alt_images[]" multiple="multiple" class="form-control">

                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-success pull-4">Add</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('script')

<script>
$(document).ready(function(){
    if ($('#noSale').is(":checked" ) )
    {
        $('#amountDiv').hide();
    }

    $('#onSale').click(function(){  
        $('#amountDiv').show();
        $('#sql_price').attr("required","required");
    });
    $('#noSale').click(function(){ 
        $('#amountDiv').hide();
    });
});
</script>
@endsection


