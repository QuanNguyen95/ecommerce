@extends('admin.layout.master')

@section('content')

<section id="container" class="">
    <section id="main-content">
        <section class="wrapper">
            <div class="content-box-large col-md-5">
                <h1>{{ $proInfo->pro_name }}</h1>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $err)
                        <li>{{ $err }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if (session('message'))
                <div class="alert alert-success col-lg-12">
                    {{session('message')}}
                </div>
                @endif
                @php
                $altImages = DB::table('alt_images')->where('id_product', $proInfo->id)->get();
                @endphp
                @if(count($altImages)>0)
                <table class="table table-striped table-hover">
                    <tr>
                        <td>ID</td>
                        <td>Alt Image</td>
                        <td>Delete</td>
                    </tr>
                    @foreach($altImages as $img)
                    <tr>
                        <td>{{$img->id}}</td>
                        <td><img src="{{ url('') }}/public/image/alt_images/{{$img->alt_img}}" style=" width:100px"/>
                        </td>
                        <td>
                            <a onclick="return window.confirm('Are you sure?');" href="{{ url('admin/product',$proInfo->id) }}/deleteAlt/{{$img->id}}" class="btn btn-danger">Remove</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
                @else
                <p class="alert alert-danger">This product have not any alt images</p>
                @endif
            </div>

            <div class="content-box-large col-md-7 pull-right">
                <h1>Add Alt Images</h1>

                <form action="{{ url('admin/product/addAlt',$proInfo->id) }}" method="post" accept-charset="utf-8" enctype='multipart/form-data'>

                    {{ csrf_field() }}

                    <table class="table-borderless" style="height:200px">

                        <tr>
                            <td> Product Name:</td>
                            <td>    
                                <input type="text" name="pro_name" class="form-control"
                                value="{{$proInfo->pro_name}}" readonly="readonly">
                                <input type="hidden" name="pro_id" class="form-control"
                                value="{{$proInfo->id}}">
                            </td>
                        </tr>

                        <tr>
                            <td> Upload Image:</td>
                            <td>    
                                <input type="file" name="image" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Submit" class="btn btn-success pull-right">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </section>
    </section>
</section>

@endsection