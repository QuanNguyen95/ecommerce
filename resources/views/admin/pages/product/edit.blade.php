@extends('admin.layout.master')

@section('content')

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-6">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title">Edit Product</div>
                    </div>
                    <div class="panel-body">

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $err)
                                <li>{{ $err }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @if (session('message'))
                        <div class="alert alert-success col-lg-12" id="message">
                            {{session('message')}}
                        </div>
                        @endif

                        <form action="{{ route('product_edit', $product->id) }}" method="post" accept-charset="utf-8" enctype='multipart/form-data'>
                            Category: 
                            <select class="form-control" name="cat_id">
                                @foreach($category as $cate)
                                <option 
                                    @if ($cate->id == $product->id_cate)
                                        selected="selected"
                                    @endif
                                value="{{ $cate->id }}">{{ $cate->name }}</option>
                                @endforeach
                            </select>
                            <br>

                            Name:    <input type="text" name="pro_name" class="form-control" value="{{ old('pro_name', $product->pro_name) }}" required="required" />
                            <br/>
                            Price     <input type="number" name="pro_price" class="form-control" value="{{ old('pro_price', $product->pro_price) }}" required="required" step="0.01" min="0" />
                            <br/>

                            Code:    <input type="text" name="pro_code" class="form-control" value="{{ old('pro_code', $product->pro_code) }}" required="required" readonly="readonly" />
                            <br/>
                            Imgage:    <img src="{{ asset('public/image/product') }}/{{$product->pro_img}}" alt="" style="width: 100px"> <input type="file" name="pro_img" class="form-control" >
                            <br/>

                            Details:    <textarea name="pro_info" class="form-control ckeditor" rows="5" required="required">{{ old('pro_info',$product->pro_info ) }}</textarea>
                            <br/>

                            <input type="radio" name="sale" value="1" id="onSale" 
                            @if ($product->sale == 1)
                                checked="checked"
                            @endif> Yes 
                            <input type="radio" name="sale" value="0" id="noSale"
                            @if ($product->sale == 0)
                                checked="checked"
                            @endif> No 
                            <br>
                            <div id="amountDiv">
                                Sale  Price:     <input id="sql_price" type="number" name="sql_price" class="form-control" value="{{old('sql_price',$product->sql_price )}}" step="0.01" min="0">
                                <br/>
                            </div>
                            
                            <br>

                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-success pull-4">Edit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('script')

<script>
$(document).ready(function(){
    if ($('#noSale').is(":checked" ) )
    {
        $('#amountDiv').hide();
    }

    $('#onSale').click(function(){  
        $('#amountDiv').show();
        $('#sql_price').attr("required","required");
    });
    $('#noSale').click(function(){ 
        $('#amountDiv').hide();
    });
});
</script>
@endsection


