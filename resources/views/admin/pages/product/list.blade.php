@extends('admin.layout.master')

@section('content')

<section id="main-content">
    <section class="wrapper">
        <div class="content-box-large">
            <h1>Products</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $err)
                            <li>{{ $err }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (session('message'))
                <div class="alert alert-success col-lg-12" id="message">
                    {{session('message')}}
                </div>
            @endif

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>Image</th>
                        <th>Catgeory</th>
                        <th>Product Name</th>
                        <th>Product Code</th>
                        <th>Product Price</th>
                        <th>Alt Images</th>
                        <th>On Sale</th>
                        <th>Update</th>
                        <th>Delete</th>
                    </tr>
                </thead>
            @foreach($product as $pro)
                <tbody>
                    <tr>
                        <td>{{$pro->id}}</td>
                        <td> <img src="{{ url('') }}/public/image/product/{{ $pro->pro_img }}" alt=""
                           width="50px" height="50px"/></td>
                        <td>{{$pro->categories->name}}</td>
                        <td>{{$pro->pro_name}}</td>
                        <td>{{$pro->pro_code}}</td>
                        <td>{{$pro->pro_price}} $</td>
                        <td>
                            @php
                                $Aimgs = DB::table('alt_images')->where('id_product', $pro->id)->get();
                            @endphp
                            
                            <p> {{count($Aimgs)}} images found</p>
                            <a href="{{ url('/admin/product/addAlt') }}/{{$pro->id}}" class="btn btn-info" style="border-radius:20px;">
                            <i class="fa fa-eye"></i> View</a>
                        </td>

                        <td>
                            <div id="checkSale{{$pro->id}}">
                                <input type="radio" name="sale{{$pro->id}}" value="1" id="onSale{{$pro->id}}" @if ($pro->sale == 1)
                                    checked="checked"
                                @endif > Yes 
                                <input type="radio" name="sale{{$pro->id}}" value="0" id="noSale{{$pro->id}}" @if ($pro->sale == 0)
                                    checked="checked"
                                @endif> No<br>
                            </div>
                            
                            <div id="amountDiv{{$pro->id}}">
                                <input type="hidden" id="pro_id{{$pro->id}}"
                                value="{{$pro->id}}"/>
                                
                                <input type="number" id="sql_price{{$pro->id}}"
                                placeholder="Sale Price" value="{{$pro->sql_price}}" style="width: 40%;">$<br>
                                <button id="saveAmount{{$pro->id}}" class="btn btn-success">
                                Save Amount</button>
                            </div>
                        </td>

                        <td><a href="{{ route('product_edit',$pro->id) }}" class="btn btn-success btn-small">Edit</a></td>
                        <td>
                            <a onclick="return window.confirm('Are you sure?');" href="{{ route('product_delete',$pro->id) }}" class="btn btn-danger">Remove</a>
                        </td>
                    </tr>
                </tbody>
            @endforeach
            </table>
            <div class="row text-center">
                {{ $product->links() }}
            </div>
        </div>
    </section>
</section>

@endsection
@section('script')

<script>
$(document).ready(function(){
    @foreach ($product as $pro)

    @if($pro->sale == 1) {
        $('#amountDiv{{$pro->id}}').show();
    } @else {
        $('#amountDiv{{$pro->id}}').hide();
    }
    @endif

    $('#onSale{{$pro->id}}').click(function(){  // run when admin need to add amount for sale
        $('#amountDiv{{$pro->id}}').show();
        var pro_id{{$pro->id}} = $('#pro_id{{$pro->id}}').val();
        var sale{{$pro->id}} = $('#onSale{{$pro->id}}').val();

        $.ajax({
            type: 'get',
            url: '{{ url('/admin/product/editSale') }}',
            data:{'sale':sale{{$pro->id}} , 'pro_id':pro_id{{$pro->id}} },
            success: function (response) {
                console.log(response);
            }
        });

    });
    $('#noSale{{$pro->id}}').click(function(){ // this when admin dnt need sale
        $('#amountDiv{{$pro->id}}').hide();
        var pro_id{{$pro->id}} = $('#pro_id{{$pro->id}}').val();
        var sale{{$pro->id}} = $('#noSale{{$pro->id}}').val();

        $.ajax({
            type: 'get',
            url: '{{ url('/admin/product/editSale') }}',
            data:{'sale':sale{{$pro->id}} , 'pro_id':pro_id{{$pro->id}}},
            success: function (response) {
                console.log(response);
            }
        });
    });

    $('#saveAmount{{$pro->id}}').click(function(){
        var salePrice{{$pro->id}} = $('#sql_price{{$pro->id}}').val();
        var pro_id{{$pro->id}} = $('#pro_id{{$pro->id}}').val();
        $.ajax({
            type: 'get',
            url: '{{ url('/admin/product/addSale') }}',
            data:{'salePrice':salePrice{{$pro->id}} , 'pro_id':pro_id{{$pro->id}} },
            success: function (response) {
                alert(response);
            }
        });
    });
    @endforeach
});
</script>
@endsection

