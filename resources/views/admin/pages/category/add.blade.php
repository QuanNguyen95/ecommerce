@extends('admin.layout.master')

@section('content')

    <section id="main-content">
        <section class="wrapper">

            <div class="content-box-large">
                <h1>Add Category</h1>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $err)
                                <li>{{ $err }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-md-6">
                    <form action="{{ url('admin/category/add') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Category Name</label>
                            <input class="form-control" name="cat_name" value="{{ old('cat_name') }}"
                                   placeholder="Category Name" required="required"/>
                        </div>
                        <button type="submit" class="btn btn-success pull-4">Add</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </form>
                </div>
            </div>

        </section>
    </section>

@endsection