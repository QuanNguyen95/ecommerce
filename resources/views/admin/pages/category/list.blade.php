@extends('admin.layout.master')

@section('content')

<section id="main-content">
    <section class="wrapper">
        <div class="content-box-large">
            <h1>List Categories</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $err)
                            <li>{{ $err }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            @if (session('message'))
                <div class="alert alert-success col-lg-12">
                    {{session('message')}}
                </div>
            @endif

            <table class="table table-striped">
                <thead>  <tr>
                    <th>Category ID</th>
                    <th>Category Name</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($category as $cate)
                <tr>
                    <td>{{$cate->id}}</td>
                    <td>{{$cate->name}}</td>
                    <td>
                        <a href="{{ url('admin/category/edit') }}/{{$cate->id}}" class="btn btn-info btn-small">Edit</a>
                    </td>
                    <td>
                        <a onclick="return window.confirm('Are you sure?');" href="{{ route('category_delete',$cate->id) }}" class="btn btn-danger">Remove</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    </section>
</section>

@endsection