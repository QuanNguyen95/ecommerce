@extends('admin.layout.master')

@section('content')

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-6">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title">Edit User</div>
                    </div>
                    <div class="panel-body">

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $err)
                                <li>{{ $err }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @if (session('message'))
                        <div class="alert alert-success col-lg-12" id="message">
                            {{session('message')}}
                        </div>
                        @endif

                        <form action="{{ route('users_edit', $user->id) }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" name="name" required="required" value="{{ old('name', $user->name) }}" placeholder="Username" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Nhập email" required="required" value="{{$user->email}}" readonly="readonly" />
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="changePassword" id="changePassword">
                                <label>Change Password</label>
                            </div>
                            <div class="form-group change">
                                <label>New Password</label>
                                <input type="password" class="form-control password" name="password" placeholder="Password" required="required" disabled="" />
                            </div>
                            <div class="form-group change">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control password" name="confirmPassword" placeholder="Confirm Password" required="required" disabled=""/>
                            </div>
                            @if ($user->admin == false)
                                <div class="form-group">
                                    <label>Active</label>
                                    <label class="radio-inline">
                                        <input name="active" value="1" type="radio"
                                        @if ($user->active == true)
                                            checked="checked"
                                        @endif> Yes 
                                    </label>
                                    <label class="radio-inline">
                                        <input name="active" value="0" type="radio"
                                        @if ($user->active == false)
                                            checked="checked"
                                        @endif> No 
                                    </label>
                                </div>
                            @endif
                            <button type="submit" class="btn btn-success">Edit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $(".change").hide()
            $("#changePassword").change(function(){
                if($(this).is(":checked"))
                {
                    $(".change").show(1000)
                    $(".password").removeAttr('disabled')
                }
                else
                {
                    $(".change").hide(1000)
                    $(".password").attr('disabled','')
                }
            });
        });
    </script>
@endsection