@extends('admin.layout.master')

@section('content')

<section id="main-content">
    <section class="wrapper">
        <div class="content-box-large">
            @if (session('message'))
                <div class="alert alert-success col-lg-12">
                    {{session('message')}}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger col-lg-12">
                    {{session('error')}}
                </div>
            @endif
            <h1>Users</h1>
            <table class="table table-striped table-advance table-hover">
                <thead>
                    <tr>
                        <th><i class="icon_tool"></i> ID</th>
                        <th><i class="icon_profile"></i> Name</th>
                        <th><i class="icon_mail_alt"></i> Email</th>
                        <th><i class="icon_calendar"></i> Role</th>
                        <th><i class="icon_box-empty"></i> Active</th>
                        <th><i class="icon_cogs"></i> Actions</th>
                    </tr>
                </thead>


                <tbody>

                    @foreach ($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>@if ($user->admin)
                                Admin
                            @else
                                User
                            @endif
                        </td>
                        <td>
                            @if ($user->active)
                                Yes
                            @else
                                No
                            @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-info popovers" data-trigger="hover" data-content="Edit user" data-placement="top" href="{{ route('users_edit',$user->id) }}">
                                    <i class="icon_minus_alt2"></i>
                                </a>
                                @if ($user->admin == false)
                                <a class="btn btn-danger popovers" data-trigger="hover" data-content="Remove" data-placement="top" href="{{ route('users_delete',$user->id) }}" onclick="return window.confirm('Are you sure?');">
                                    <i class="icon_close_alt2"></i>
                                </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
    </section>
</section>

@endsection

