<!DOCTYPE html>
<html>
<head>
	<title>E-SHOPPER</title>
</head>
<body>
	<p>
		Welcome {{$user->name}} has registered for E-SHOPPER. Please click on the following link to complete the registration.
		</br>
		<a href="{{ $activation_link }}">{{ $activation_link }}</a>
	</p>
</body>
</html>