<!DOCTYPE html>
<html>
<head>
	<title>E-SHOPPER</title>
</head>
<body>
	Thank you for your order

	Information:

	<table class="table table-striped">
		<thead>
			<tr class="cart_menu">
				<td class="description">Product Name</td>
				<td class="price">Price</td>
				<td class="quantity">Quantity</td>
				<td class="total">Total</td>
			</tr>
		</thead>
		<tbody>
			@foreach($cart::content() as $cartItem)
			<tr>
				<td class="cart_description">
					<h4><a href="">{{$cartItem->name}}</a></h4>
					<p>Web ID: {{$cartItem->id}}</p>
				</td>
				<td class="cart_price">
					<p>${{$cartItem->price}}</p>
				</td>
				<td class="cart_quantity">
					<div class="cart_quantity_button">
						<input class="cart_quantity_input" type="text"  value="{{$cartItem->qty}}" readonly="readonly" size="2">
					</div>
				</td>
				<td class="cart_total">
					<p class="cart_total_price">${{$cartItem->subtotal}}</p>
				</td>

			</tr>
			@endforeach
			<tr>
				<td colspan="4">&nbsp;</td>
				<td colspan="2">
					<table class="table table-condensed total-result">
						<tr>
							<td>Cart Sub Total</td>
							<td>${{Cart::subtotal()}}</td>
						</tr>
						<tr>
							<td> Tax</td>
							<td>${{Cart::tax()}}</td>
						</tr>
						<tr class="shipping-cost">
							<td>Shipping Cost</td>
							<td>Free</td>
						</tr>
						<tr>
							<td>Total</td>
							<td><span>${{Cart::total()}}</span></td>
						</tr>
					</table>
				</td>
			</tr>

		</tbody>
	</table>
</body>
</html>

