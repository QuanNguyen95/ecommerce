@extends('front.layout.master')
@section('title', 'Home | E-Shopper')

@section('content')
<section id="slider"><!--slider-->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div id="slider-carousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#slider-carousel" data-slide-to="1"></li>
						<li data-target="#slider-carousel" data-slide-to="2"></li>
					</ol>
					
					<div class="carousel-inner">
						<div class="item active">
							<div class="col-sm-6">
								<h1><span>E</span>-SHOPPER</h1>
								<h2>LÝ DO BẠN NÊN MUA HỎA QUẢ Ở LUÔN TƯƠI SẠCH E-SHOPPER </h2>
								<p>- Hoa quả luôn được đảm bảo nguồn gốc xuất xứ ( luôn có giấy chứng nhận nguồn gốc). </p>
								<p>- Đa dạng dịch vụ quà tặng, quà tết, lễ sang trọng, lịch sự...
								</p>
								<p>- Miễn phí vận chuyển cho khách hàng ở nội thành Hà Nội.</p>
							</div>
							<div class="col-sm-6">
								<img src="{{ asset('public/front/images/home/banner1.jpg') }}" class="girl img-responsive" alt="" />
								{{-- <img src="{{ asset('public/front/images/home/pricing.png') }}"  class="pricing" alt="" /> --}}
							</div>
						</div>
						<div class="item">
							<div class="col-sm-6">
								<h1><span>E</span>-SHOPPER</h1>
								<h2>LÝ DO BẠN NÊN MUA HỎA QUẢ Ở LUÔN TƯƠI SẠCH E-SHOPPER </h2>
								<p>- Hoa quả luôn được đảm bảo nguồn gốc xuất xứ ( luôn có giấy chứng nhận nguồn gốc). </p>
								<p>- Đa dạng dịch vụ quà tặng, quà tết, lễ sang trọng, lịch sự...
								</p>
								<p>- Miễn phí vận chuyển cho khách hàng ở nội thành Hà Nội.</p>
							</div>
							<div class="col-sm-6">
								<img src="{{ asset('public/front/images/home/banner2.jpg') }}" class="girl img-responsive" alt="" />
								{{-- <img src="{{ asset('public/front/images/home/pricing.png') }}"  class="pricing" alt="" /> --}}
							</div>
						</div>
						
						<div class="item">
							<div class="col-sm-6">
								<h1><span>E</span>-SHOPPER</h1>
								<h2>LÝ DO BẠN NÊN MUA HỎA QUẢ Ở LUÔN TƯƠI SẠCH E-SHOPPER </h2>
								<p>- Hoa quả luôn được đảm bảo nguồn gốc xuất xứ ( luôn có giấy chứng nhận nguồn gốc). </p>
								<p>- Đa dạng dịch vụ quà tặng, quà tết, lễ sang trọng, lịch sự...
								</p>
								<p>- Miễn phí vận chuyển cho khách hàng ở nội thành Hà Nội.</p>
							</div>
							<div class="col-sm-6">
								<img src="{{ asset('public/front/images/home/banner.jpg') }}" class="girl img-responsive" alt="" />
								{{-- <img src="{{ asset('public/front/images/home/pricing.png') }}" class="pricing" alt="" /> --}}
							</div>
						</div>
						
					</div>
					
					<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>
					<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
				
			</div>
		</div>
	</div>
</section><!--/slider-->

<section>
	<div class="container">
		<div class="row">
			@include('front.layout.left-sidebar')
			
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					<h2 class="title text-center">Features Items</h2>
					@foreach ($products as $pro)
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="{{ asset('public/image/product') }}/{{$pro->pro_img}}" alt="" />
									@if ($pro->sale)
									
									<h2><span class="col-sm-6 checksale">${{$pro->pro_price}}</span> <span class="col-sm-6">${{$pro->sql_price}}</span></h2>
									@else
                                		<h2><span class="col-sm-12" >${{$pro->pro_price}}</span></h2>
                                	@endif
									<p>{{$pro->pro_name}}</p>
									<a href="{{ route('add_cart', $pro->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
								</div>
								<div class="product-overlay">
									<div class="overlay-content">
										
										<a href="{{ route('product',[$pro->id,$pro->pro_nameURL]) }}" title=""><p>{{$pro->pro_name}}</p></a>
										<a href="{{ route('add_cart', $pro->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
									</div>
								</div>
								@if ($pro->sale)
									<img src="{{ asset('public/front/images/home/sale.png') }}" class="new" alt="" />
								@elseif($pro->created_at > \Carbon\Carbon::now()->subDay(7))
									<img src="{{ asset('public/front/images/home/new.png') }}" class="new" alt="" />
								@endif
							</div>
							{{-- <div class="choose">
								<ul class="nav nav-pills nav-justified">
									<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
									<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
								</ul>
							</div> --}}
						</div>
					</div>
					@endforeach

				</div><!--features_items-->
				
				<div class="category-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							@php $i = 0; @endphp
							@foreach ($category as $cate)
							<li @if ($i == 0) class="active" @endif>
								<a href="#{{$cate->name}}" data-toggle="tab" >{{$cate->name}}</a>
							</li>
							@php $i++; @endphp
							@endforeach
						</ul>
					</div>
					<div class="tab-content">
						@php $i = 0; @endphp
						@foreach ($category as $cate)
						<div @if ($i == 0) class="tab-pane fade active in" @else class="tab-pane fade" @endif id="{{$cate->name}}">
							@php
								$pr = $cate->product->sortByDesc('id')->take(3);
							@endphp
							@forelse ($pr as $p)
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<a href="{{ route('product',[$p->id,$p->pro_nameURL]) }}" title=""><img src="{{ asset('public/image/product') }}/{{$p->pro_img}}" alt="" /></a>
											@if ($p->sale)
											<h2><span class="col-sm-6 checksale">${{$p->pro_price}}</span> <span class="col-sm-6">${{$p->sql_price}}</span></h2>
											@else
		                                		<h2><span class="col-sm-12" >${{$p->pro_price}}</span></h2>
		                                	@endif
											<a href="{{ route('product',[$p->id,$p->pro_nameURL]) }}" title=""><p>{{$p->pro_name}}</p></a>
											<a href="{{ route('add_cart', $p->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
										@if ($p->sale)
											<img src="{{ asset('public/front/images/home/sale.png') }}" class="new" alt="" />
										@elseif($p->created_at > \Carbon\Carbon::now()->subDay(7))
											<img src="{{ asset('public/front/images/home/new.png') }}" class="new" alt="" />
										@endif
										
									</div>
								</div>
							</div>
							@empty
							<div class="alert text-danger text-center">
                        		<h3>Sorry, products not found</h3>
                    		</div>
							@endforelse
						</div>
						@php $i++; @endphp
						@endforeach				
					</div>
				</div><!--/category-tab-->
				
				@include('front.layout.recommended_items')
				<div class="row text-center">
					{{ $products->links() }}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection