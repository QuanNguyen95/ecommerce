@extends('front.layout.master')
@section('title', 'Product | E-Shopper')

@section('content')

<section>
    <div class="container">
        <div class="row">
            @include('front.layout.left-sidebar')
            
            <div class="col-sm-9 padding-right">
                <div class="features_items"> <!--features_items-->
                    <b> Total Products</b>: {{$products->count()}}
                    <h2 class="title text-center">
                        Features Item
                    </h2>

                    @forelse ($products as $pro)                  
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{ asset('public/image/product') }}/{{$pro->pro_img}}" alt="" />
                                    @if ($pro->sale)
                                    
                                    <h2><span class="col-sm-6 checksale">${{$pro->pro_price}}</span> <span class="col-sm-6">${{$pro->sql_price}}</span></h2>
                                    @else
                                        <h2><span class="col-sm-12" >${{$pro->pro_price}}</span></h2>
                                    @endif
                                    <a href="{{ route('product',[$pro->id,$pro->pro_nameURL]) }}" title=""><p>{{$pro->pro_name}}</p></a>
                                    <a href="{{ route('add_cart', $pro->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                </div>
                                <div class="product-overlay">
                                    <div class="overlay-content">
                                        <a href="{{ route('product',[$pro->id,$pro->pro_nameURL]) }}" title=""><p>{{$pro->pro_name}}</p></a>
                                        <a href="{{ route('add_cart', $pro->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </div>
                                </div>
                                @if ($pro->sale)
                                <img src="{{ asset('public/front/images/home/sale.png') }}" class="new" alt="" />
                                @elseif($pro->created_at > \Carbon\Carbon::now()->subDay(7))
                                <img src="{{ asset('public/front/images/home/new.png') }}" class="new" alt="" />
                                @endif
                            </div>
                            {{-- <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                </ul>
                            </div> --}}
                        </div>
                    </div>
                    @empty
                    <div class="alert alert-danger">
                        <h3>Sorry, products not found</h3>
                    </div>
                    @endforelse
                </div>

                <div class="row text-center">
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
