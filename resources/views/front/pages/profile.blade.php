
@extends('front.layout.master')
@section('title', 'Profile | E-Shopper')

@section('content')

<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">My Profile</li>
            </ol>
        </div><!--/breadcrums-->

        <div class="row">
            <div class="col-md-4 well well-sm">

                <ul class="nav navbar">
                    <h3 class="">Quick Links</h3>
                    <li><a href="{{url('/profile.html')}}">My Profile</a></li>
                    <li><a href="{{url('/orders.html')}}">My Orders</a></li>
                </ul>
            </div>
            <div class="col-md-7 well well-sm col-md-offset-1">
                <h3><span style='color:green'>{{ucwords(Auth::user()->name)}}</span>, Welcome</h3>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        <li>{{ $errors->first() }}</li>
                    </ul>
                </div>
                @endif
                @if (session('message'))
                <div class="alert alert-success col-lg-12" id="message">
                    {{session('message')}}
                </div>
                @endif
                <form action="{{ route('profile') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_user" value="{{Auth::id()}}">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" name="name" required="required" value="{{Auth::user()->name}}" placeholder="Username" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" required="required" value="{{Auth::user()->email}}" disabled="disabled" />
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="changePassword" id="changePassword">
                        <label>Change Password</label>
                    </div>
                    <div class="form-group change">
                        <label>New Password</label>
                        <input type="password" class="form-control password" name="password" placeholder="Password" required="required" disabled="" />
                    </div>
                    <div class="form-group change">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control password" name="confirmPassword" placeholder="Confirm Password" required="required" disabled=""/>
                    </div>

                    <button type="submit" class="btn btn-success">Edit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $(".change").hide()
            $("#changePassword").change(function(){
                if($(this).is(":checked"))
                {
                    $(".change").show(1000)
                    $(".password").removeAttr('disabled')
                }
                else
                {
                    $(".change").hide(1000)
                    $(".password").attr('disabled','')
                }
            });
        });
    </script>
@endsection
