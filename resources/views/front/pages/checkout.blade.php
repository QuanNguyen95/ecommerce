@extends('front.layout.master')
@section('title', 'Checkout | E-Shopper')

@section('content')
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="#">Home</a></li>
				<li class="active">Check out</li>
			</ol>
		</div><!--/breadcrums-->

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $err)
                        <li>{{ $err }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
		
		<form action="{{ route('checkout') }}" method="POST">
			{{csrf_field()}}
			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-8">
						<div class="bill-to">
							<p>Bill To</p>
							<div class="form-one">
								
								<input type="text" placeholder="Full Name *" required="" name="name" class="checkout">
								<input type="email" placeholder="Email *" required="" name="email" class="checkout">
								<input type="text" placeholder="Phone *" required="" name="phone" class="checkout">
								
							</div>
							<div class="form-two">
								<div class="order-message">
									<textarea name="address" rows="10" placeholder="Shipping address" required=""></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="order-message">
							<p>Shipping Order</p>
							<textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="10"></textarea>
						</div>	
					</div>					
				</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
						</tr>
					</thead>
					<tbody>
						@foreach($cartItems as $cartItem)
						<tr>
							<td class="cart_product">
								<a href="{{ route('product',[$cartItem->id,$cartItem->options['nameURL']]) }}"><img src="{{ asset('public/image/product') }}/{{$cartItem->options['image']}}" alt="" width="200px"></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$cartItem->name}}</a></h4>
								<p>Web ID: {{$cartItem->id}}</p>
							</td>
							<td class="cart_price">
								<p>${{$cartItem->price}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">

									<input class="cart_quantity_input" type="text"  value="{{$cartItem->qty}}" readonly="readonly" size="2">

								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">${{$cartItem->subtotal}}</p>
							</td>

						</tr>
						@endforeach
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>${{Cart::subtotal()}}</td>
									</tr>
									<tr>
										<td> Tax</td>
										<td>${{Cart::tax()}}</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>
									</tr>
									<tr>
										<td>Total</td>
										<td><span>${{Cart::total()}}</span></td>
									</tr>
								</table>
							</td>
						</tr>

					</tbody>
				</table>
			</div>
			@if (Cart::count() > 0)
			<div class="payment-options">
				<span>
					<input type="radio" name="pay" value="COD" checked="checked" id="cash"> COD

				</span>
				<span style="margin-left: 20px">
					<input type="radio" name="pay" value="paypal" id="paypal"> PayPal
				</span>

				<p style="margin-top: 20px">
					<input type="submit" value="COD" class="btn btn-success" id="cashbtn">
					@include('front.cart.paypal')
				</p>
			</div>
			@endif
		</form>
	</div>
</section> <!--/#cart_items-->
@endsection
@section('script')
<script>

	$(document).ready(function(){
	    if ($(':radio[id=cash]').is( ":checked" )) {
	    	$('#paypalbtn').hide();
			$('#cashbtn').show();
	    }
	    if ($(':radio[id=paypal]').is( ":checked" )) {
	    	$('#paypalbtn').show();
			$('#cashbtn').hide();
	    }

	    $(':radio[id=paypal]').change(function(){
		$('#paypalbtn').show();
		$('#cashbtn').hide();

	});

		$(':radio[id=cash]').change(function(){
		$('#paypalbtn').hide();
		$('#cashbtn').show();
		});
	});

</script>
@endsection