@extends('front.layout.master')
@section('title', 'Login | E-Shopper')

@section('content')
<section id="form"><!--form-->
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-sm-offset-1">
				<div class="login-form"><!--login form-->
					<h2>Login to your account</h2>

					@if (session('message'))
					<div class="alert alert-danger">
						{{session('message')}}
					</div>
					@endif 
					<form action="{{ route('login') }}" method="POST">
						{{csrf_field()}}
						<input type="email" placeholder="Email Address" name="email"/>
						<input type="password" placeholder="Password" name="password"/>
						<span>
							<input type="checkbox" class="checkbox"> 
							Keep me signed in
						</span>
						<button type="submit" class="btn btn-default">Login</button>
					</form>
				</div><!--/login form-->
			</div>
			<div class="col-sm-1">
				<h2 class="or">OR</h2>
			</div>
			<div class="col-sm-4">
				<div class="signup-form"><!--sign up form-->
					<h2>New User Signup!</h2>
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							<li>{{ $errors->first() }}</li>
						</ul>
					</div>
					@endif
					@if (session('status'))
					<div class="alert alert-info">
						{{session('status')}}
					</div>
					@endif
					@if (session('error'))
					<div class="alert alert-danger">
						{{session('error')}}
					</div>
					@endif 
					<form action="{{ route('signup') }}" method="POST" id="form_signup">
						{{csrf_field()}}
						<input type="text" placeholder="Name" name="name_signup" />
						<input type="email" placeholder="Email Address" name="email_signup"/>
						<input type="password" placeholder="Password" name="password_signup"/>
						<input type="password" placeholder="Confirrm Password" name="confirmpassword_signup"/>
						<div class="g-recaptcha" data-sitekey="6Ldf9VwUAAAAAPgvf4ngeSWMG1UoP5F2sILsDPtS"></div>
						<button type="submit" class="btn btn-default" style="margin-top: 10px;">Signup</button>

					</form>
				</div><!--/sign up form-->
			</div>
		</div>
	</div>
</section><!--/form-->

@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function(){
    	$('#form_signup').submit(function(event){
        	var verified = grecaptcha.getResponse();
          	if (verified.length === 0) {
            	event.preventDefault();
          	}
    	});
	});
</script>
@endsection
