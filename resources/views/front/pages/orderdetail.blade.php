@extends('front.layout.master')
@section('title', 'Order | E-Shopper')

@section('content')
<style>
table td { padding:10px
}</style>



<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="">Profile</a></li>
				<li class="active">Order Detail</li>
			</ol>
		</div><!--/breadcrums-->



		<div class="row">
			<div class="col-md-3 well well-sm">

				<ul class="nav navbar">
					<h3 class="">Quick Links</h3>
					<li><a href="{{url('/profile.html')}}">My Profile</a></li>
					<li><a href="{{url('/orders.html')}}">My Orders</a></li>
				</ul>
			</div>
			<div class="col-md-8 well well-sm col-md-offset-1">
				<h3 ><span style='color:green'>{{ucwords(Auth::user()->name)}}</span>,  Your Orders</h3>
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
						</tr>
					</thead>
					<tbody>
						@php
							$subtotal = 0;
							$tax = 0;
						@endphp
						@foreach($order_details as $order_detail)
						@php
							$subtotal += $order_detail->price*$order_detail->quantity;
							$tax += $order_detail->tax*$order_detail->quantity;
						@endphp
						<tr>
							<td class=""><a href="{{ route('product',[$order_detail->id_product, $order_detail->product->pro_nameURL]) }}" title=""><img src="{{ asset('public/image/product') }}/{{$order_detail->product->pro_img}}" alt="" width="100px"></a></td>
							<td class="cart_description"><h4>{{$order_detail->product->pro_name}}</h4>
							<p>Web ID: {{$order_detail->product->pro_code}}</p></td>
							<td class="cart_price">{{$order_detail->price}}</td>
							<td class="cart_quantity">{{$order_detail->quantity}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="col-sm-4 col-md-offset-8" style="margin-bottom: 50px">
				<div class="total_area">
					<ul>
						<li>Cart Sub Total <span>${{$subtotal}}</span></li>
						<li>Tax <span>$tax</span></li>
						<li>Shipping Cost <span>Free</span></li>
						<li>Total <span>${{$order->total}}</span></li>
					</ul>
				</div>
			</div>
		</div>

	</div>
</section>

@endsection