@extends('front.layout.master')
@section('title', 'Product Detail | E-Shopper')

@section('content')

<section>
    <div class="container">
        <div class="row">
            @include('front.layout.left-sidebar')
            
            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-5">
                        <div class="view-product">
                            <img src="{{ asset('public/image/product') }}/{{$pro->pro_img}}" alt=""  style="height: auto;" />
                            
                        </div>
                        <div id="similar-product" class="carousel slide" data-ride="carousel">

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                @php
                                    $i = 0;
                                    $count = 0;
                                @endphp
                                @for ($j = 0; $j < ceil(count($altImgs)/3); $j++)
                                <div @if ($i == 0) class="item active" @endif class="item">
                                    @php
                                        $altImg = DB::table('alt_images')->where('id_product', $pro->id)->skip($count)->take(3)->get();
                                    @endphp
                                    @foreach($altImg as $alt)
                                    <img src="{{URL::to('/')}}/public/image/alt_images/{{$alt->alt_img}}" style="width:84px; height:84px;"/>
                                    @endforeach
                                    @php
                                        $count += 3;
                                    @endphp
                                </div>
                                @php
                                    $i++
                                @endphp
                                @endfor

                            </div>

                            <!-- Controls -->
                            <a class="left item-control" href="#similar-product" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right item-control" href="#similar-product" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                            @if($pro->created_at > \Carbon\Carbon::now()->subDay(7))
                            <img src="{{ asset('public/front/images/product-details/new.png') }}" class="newarrival" alt="" />
                            @endif
                            @if($pro->sale)
                            <img src="{{ asset('public/front/images/product-details/sale.png') }}" style="position: absolute; top: 0; left: 120px;" alt="" />
                            @endif
                            
                            <a href="{{ route('product',[$pro->id,$pro->pro_nameURL]) }}" title=""><h2>{{$pro->pro_name}}</h2></a>
                            <p>Web ID: {{$pro->pro_code}}</p>
                            <img src="{{ asset('public/front/images/product-details/rating.png') }}" alt="" />
                            <span>
                                @if ($pro->sale)
                                <p style="text-decoration: line-through; font-size: 18px;">${{$pro->pro_price}}</p>
                                <span>${{$pro->sql_price}}</span>
                                @else
                                <span>${{$pro->pro_price}}</span>
                                @endif
                                <label>Quantity:</label>
                                <input type="number" value="1" min="1" max="30" name="qty" id="qty"/>
                                <input type="hidden" name="pro_id" value="{{$pro->id}}" id="pro_id">
                                <button type="button" class="btn btn-fefault cart" id="addToCart">
                                    <i class="fa fa-shopping-cart"></i>
                                    Add to cart
                                </button>
                            </span>
                            
                            <p><b>Unit:</b> 1 kg</p>
                            <p><b>Availability:</b> In Stock</p>
                            <p><b>Condition:@if ($pro->sale)
                                Sale 
                                @endif
                                @if($pro->created_at > \Carbon\Carbon::now()->subDay(7))
                                New
                                @endif 
                                @if (!$pro->sale AND $pro->created_at <= \Carbon\Carbon::now()->subDay(7))
                                None
                                @endif
                            </b></p>
                            <p><b>Brand:</b> E-SHOPPER</p>
                            <a href=""><img src="{{ asset('public/front/images/product-details/share.png') }}" class="share img-responsive"  alt="" /></a>
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->

                <div class="category-tab shop-details-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li><a href="#details" data-toggle="tab">Details</a></li>
                            <li class="active"><a href="#reviews" data-toggle="tab">Reviews ({{$comments->count()}})</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade active" id="details" >
                            <div class="col-sm-10 col-sm-push-1">
                                <p>{!! $pro->pro_info !!}</p>
                            </div>
                        </div>
                        <div class="tab-pane fade in" id="reviews" >
                            <div class="col-sm-12">
                                @foreach ($comments as $comment)
                                <ul>
                                    <li><a href="javascript:;"><i class="fa fa-user"></i>{{$comment->user->name}}</a></li>
                                    <li><a href="javascript:;"><i class="fa fa-clock-o"></i>{{$comment->created_at->format('H:i A')}}</a></li>
                                    <li><a href="javascript:;"><i class="fa fa-calendar-o"></i>{{$comment->created_at->format('d M Y') }}</a></li>
                                </ul>
                                <p>{{$comment->comment}}</p>
                                <hr>
                                @endforeach
                                <p><b>Write Your Review</b></p>
                                @if (Auth::check())
                                <form action="{{ route('review') }}" method="POST">
                                    {{csrf_field()}}
                                    <span>
                                        <input type="text" placeholder="Your Name"/ value="{{Auth::user()->name}}" readonly="">
                                        <input type="email" placeholder="Email Address" value="{{Auth::user()->email}}"/ readonly="">
                                    </span>
                                    <textarea name="comment" ></textarea>
                                    <b>Rating: 
                                    <fieldset class="rating">
                                        <input type="radio" id="star5" name="rating" value="5" required="" />
                                        <label class = "full" for="star5" title="Awesome - 5 stars"></label>

                                        <input type="radio" id="star4half" name="rating" value="4.5" required=""/>
                                        <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>

                                        <input type="radio" id="star4" name="rating" value="4" required=""/>
                                        <label class = "full" for="star4" title="Pretty good - 4 stars"></label>

                                        <input type="radio" id="star3half" name="rating" value="3.5" required=""/>
                                        <label class="half" for="star3half" title="Meh - 3.5 stars"></label>

                                        <input type="radio" id="star3" name="rating" value="3" required=""/>
                                        <label class = "full" for="star3" title="Meh - 3 stars"></label>

                                        <input type="radio" id="star2half" name="rating" value="2.5" required=""/>
                                        <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>

                                        <input type="radio" id="star2" name="rating" value="2" required=""/>
                                        <label class = "full" for="star2" title="Kinda bad - 2 stars"></label>

                                        <input type="radio" id="star1half" name="rating" value="1.5" required=""/>
                                        <label class="half" for="star1half" title="Meh - 1.5 stars"></label>

                                        <input type="radio" id="star1" name="rating" value="1" required=""/>
                                        <label class = "full" for="star1" title="Sucks big time - 1 star"></label>

                                        <input type="radio" id="starhalf" name="rating" value="0.5" required=""/>
                                        <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                    </fieldset>
                                    </b> 
                                    <button type="submit" class="btn btn-default pull-right">
                                        Submit
                                    </button>
                                </form>
                                @else
                                    Please enter login to rating
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div><!--/category-tab-->

                @include('front.layout.recommended_items')

            </div>

        </div>
    </div>
</section>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('#addToCart').click(function(event){
            // event.preventDefault();
            var qty = $('#qty').val();
            var pro_id = $('#pro_id').val();

            $.ajax({
                type: 'get',
                dataType: "json",
                url: '{{ url('cart/addAjax.html') }}',
                data:{'qty':qty , 'pro_id':pro_id }, 
                success: function (data) {
                    if(data.status=='TRUE'){
                        $('#totalProductCart').html(data.totalProductCart);
                        alert('Add product to cart successfully');
                    }
                }
            });
        });

        
    });
</script>
@endsection