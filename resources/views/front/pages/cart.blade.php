@extends('front.layout.master')
@section('title', 'Cart | E-Shopper')

@section('content')
{{-- @php
	echo "<pre>";
	var_dump(Cart::content());
	echo "</pre>";
@endphp --}}
@if ($cartItems->isEmpty())
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="{{url('/')}}">Home</a></li>
				<li class="active">Shopping Cart</li>
			</ol>
		</div>
		<div class="text-center" style="margin-bottom: 50px;">  <img src="{{asset('public/front/images/cart/empty-cart.png')}}"/></div>
	</div>
</section> <!--/#cart_items-->
@else
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="{{ route('index') }}">Home</a></li>
				<li class="active">Shopping Cart</li>
			</ol>
		</div>
		<div class="table-responsive cart_info">
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">Item</td>
						<td class="description"></td>
						<td class="price">Price</td>
						<td class="quantity">Quantity</td>
						<td class="total">Total</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					@foreach($cartItems as $cartItem)
					<tr>
						<input type="hidden" value="{{$cartItem->id}}" >
						<input type="hidden" id="rowId{{$cartItem->id}}" value="{{$cartItem->rowId}}" >

						<td class="cart_product">
							<a href="{{ route('product',[$cartItem->id,$cartItem->options['nameURL']]) }}"><img src="{{ asset('public/image/product') }}/{{$cartItem->options['image']}}" alt="" width="200px"></a>
						</td>
						<td class="cart_description">
							<h4><a href="{{ route('product',[$cartItem->id,$cartItem->options['nameURL']]) }}">{{$cartItem->name}}</a></h4>
							<p>Web ID: {{$cartItem->options['code']}}</p>
						</td>
						<td class="cart_price">
							<p>${{$cartItem->price}}</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button">
								<a class="cart_quantity_down" href="javascript:void(0)" id="cart_quantity_down{{$cartItem->id}}"> - </a>
								
								<input class="cart_quantity_input" type="number" name="quantity" value="{{$cartItem->qty}}" autocomplete="off" size="2" min="1" id="qty{{$cartItem->id}}" />
								<a class="cart_quantity_up" href="javascript:void(0)" id="cart_quantity_up{{$cartItem->id}}"> + </a>
							</div>
						</td>
						<td class="cart_total">
							<p class="cart_total_price" id="cartItemPrice{{$cartItem->id}}">${{$cartItem->subtotal()}}</p>
						</td>
						<td class="cart_delete">
							<a class="cart_quantity_delete" href="{{ route('remove_cart',$cartItem->rowId) }}" onclick="window.confirm('Are you sure?')" ><i class="fa fa-times"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section> <!--/#cart_items-->

<section id="do_action">
	<div class="container">
		<div class="heading">
			<h3>What would you like to do next?</h3>
			<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="chose_area">
					<ul class="user_option">
						<li>
							<input type="checkbox">
							<label>Use Coupon Code</label>
						</li>
						<li>
							<input type="checkbox">
							<label>Use Gift Voucher</label>
						</li>
						<li>
							<input type="checkbox">
							<label>Estimate Shipping & Taxes</label>
						</li>
					</ul>
					<ul class="user_info">
						<li class="single_field">
							<label>Country:</label>
							<select>
								<option>United States</option>
								<option>Bangladesh</option>
								<option>UK</option>
								<option>India</option>
								<option>Pakistan</option>
								<option>Ucrane</option>
								<option>Canada</option>
								<option>Dubai</option>
							</select>

						</li>
						<li class="single_field">
							<label>Region / State:</label>
							<select>
								<option>Select</option>
								<option>Dhaka</option>
								<option>London</option>
								<option>Dillih</option>
								<option>Lahore</option>
								<option>Alaska</option>
								<option>Canada</option>
								<option>Dubai</option>
							</select>
							
						</li>
						<li class="single_field zip-field">
							<label>Zip Code:</label>
							<input type="text">
						</li>
					</ul>
					<a class="btn btn-default update" href="#">Get Quotes</a>
					<a class="btn btn-default check_out" href="#">Continue</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="total_area">
					<ul>
						<li>Cart Sub Total <span>${{Cart::subtotal()}}</span></li>
						<li>Eco Tax <span>${{Cart::tax()}}</span></li>
						<li>Shipping Cost <span>Free</span></li>
						<li>Total <span>${{Cart::total()}}</span></li>
					</ul>
					<a class="btn btn-default update" href="#">Update</a>
					<a class="btn btn-default check_out" href="{{ route('checkout') }}">Check Out</a>
				</div>
			</div>
		</div>
	</div>
</section><!--/#do_action-->
@endif
@endsection

@section('script')
<script type="text/javascript">

	$(document).ready(function(){
		@foreach ($cartItems as $cartItem)

		$("#qty{{$cartItem->id}}").change(function() {
        	var rowId = $("#rowId{{$cartItem->id}}").val();
        	var qty = $("#qty{{$cartItem->id}}").val();

	        $.ajax({
                url: '{{ route('update_cart') }}',
                dataType: "json",
                type: 'get',
                data: {'rowId':rowId , 'qty':qty },
                success: function(data){ 
                	if(data.status=='TRUE'){
                		alert('Edit Cart successfully');
                        window.location = '{{ route('cart') }}';
                    } else {
                    	alert('An error occurred. Please try again');
                    }
				},
				error: function(){
					alert('Error!');
				}
            });
		});

		$("#cart_quantity_down{{$cartItem->id}}").click(function() {
        	var rowId = $("#rowId{{$cartItem->id}}").val();
        	var qty = parseInt($("#qty{{$cartItem->id}}").attr('value'), 10) -1;

	        $.ajax({
                url: '{{ route('update_cart') }}',
                dataType: "json",
                type: 'get',
                data: {'rowId':rowId , 'qty':qty },
                success: function(data){ 
                	if(data.status=='TRUE'){
                        window.location = '{{ route('cart') }}';
                    } else {
                    	alert('An error occurred. Please try again');
                    }
				},
				error: function(){
					alert('Error!');
				}

            });
		});

		$("#cart_quantity_up{{$cartItem->id}}").click(function() {
        	var rowId = $("#rowId{{$cartItem->id}}").val();
        	var qty = parseInt($("#qty{{$cartItem->id}}").attr('value'), 10) + 1;

	        $.ajax({
                url: '{{ route('update_cart') }}',
                dataType: "json",
                type: 'get',
                data: {'rowId':rowId , 'qty':qty },
                success: function(data){ 
                	if(data.status=='TRUE'){
                        window.location = '{{ route('cart') }}';
                    } else {
                    	alert('An error occurred. Please try again');
                    }
				},
				error: function(){
					alert('Error!');
				}
            });
		});

		@endforeach
	});

</script>
@endsection