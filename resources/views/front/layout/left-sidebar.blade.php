<div class="col-sm-3">
	<div class="left-sidebar">

		<div class="brands_products"><!--category-products-->
			<h2>Catrgory</h2>
			<div class="brands-name">
				<ul class="nav nav-pills nav-stacked">
					@foreach ($category as $cate)
					@php
						$product = $cate->product->count();
					@endphp
					<li><a href="{{ route('category',$cate->nameURL) }}"> <span class="pull-right">({{$product}})</span>{{ $cate->name }}</a></li>
					@endforeach
				</ul>
			</div>
		</div><!--/category-products-->
		
		<div class="price-range"><!--price-range-->
			<h2>Price Range</h2>
			<div class="well text-center">
				<input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
				<b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
			</div>
		</div><!--/price-range-->
		
		<div class="shipping text-center"><!--shipping-->
			<img src="{{ asset('public/front/images/home/shipping.jpg') }}" alt="" />
		</div><!--/shipping-->
		
	</div>
</div>