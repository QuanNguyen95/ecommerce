<div class="header-middle"><!--header-middle-->
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<div class="logo pull-left">
					<a href="{{ route('index') }}"><img src="{{ asset('public/front/images/home/logo.png') }}" alt="" /></a>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="shop-menu pull-right">
					<ul class="nav navbar-nav">
						@if (Auth::check())
							<li><a href="{{ route('profile') }}" class="{{ request()->is('profile.html') ? 'active' : '' }}"><i class="fa fa-user"></i> {{Auth::user()->name}}</a></li>
						@endif

						{{-- <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li> --}}
						<li><a href="{{ route('checkout') }}" class="{{ request()->is('checkout.html') ? 'active' : '' }}"><i class="fa fa-crosshairs"></i> Checkout</a></li>
						<li><a href="{{ route('cart') }}" class="{{ request()->is('cart.html') ? 'active' : '' }}"><i class="fa fa-shopping-cart"></i> Cart (<span id="totalProductCart">{{Cart::count()}}</span>)</a></li>
						
						@if (Auth::check())
						<li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
						@else
						<li><a href="{{ route('login') }}" class="{{ request()->is('login.html') ? 'active' : '' }}"><i class="fa fa-lock"></i> Login</a></li>
						@endif
						
					</ul>
				</div>
			</div>
		</div>
	</div>
</div><!--/header-middle-->