<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link href="{{ asset('public/front/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/front/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/front/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('public/front/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('public/front/css/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('public/front/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('public/front/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('public/front/css/mycss.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="{{ asset('public/front/images/ico/icon.jpg') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('public/front/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('public/front/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('public/front/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('public/front/images/ico/apple-touch-icon-57-precomposed.png') }}">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head><!--/head-->

<body>
	<header id="header"><!--header-->

		@include('front.layout.top_header')
		
		@include('front.layout.middle_header')
	
		@include('front.layout.menu')
	</header><!--/header-->
	
	@yield('content')
	
	@include('front.layout.footer')
	

  
    <script src="{{ asset('public/front/js/jquery.js') }}"></script>
	<script src="{{ asset('public/front/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('public/front/js/jquery.scrollUp.min.js') }}"></script>
	<script src="{{ asset('public/front/js/price-range.js') }}"></script>
    <script src="{{ asset('public/front/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('public/front/js/main.js') }}"></script>

    @yield('script')
</body>
</html>