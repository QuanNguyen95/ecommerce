<div class="header-bottom"><!--header-bottom-->
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="mainmenu pull-left">
					<ul class="nav navbar-nav collapse navbar-collapse">
						<li><a href="{{ route('index') }}" class="{{ \Request::is('/') ? 'active' : '' }}">Home</a></li>
						<li class="dropdown"><a href="#" class="{{ request()->is('category/*') ? 'active' : '' }}">Category<i class="fa fa-angle-down"></i></a>
							<ul role="menu" class="sub-menu">
								@foreach ($category as $cate)
									<li><a href="{{ route('category',$cate->nameURL) }}">{{ $cate->name }}</a></li>
								@endforeach
								
							</ul>
						</li> 
						<li><a href="{{ route('contact') }}" class="{{ request()->is('contact.html') ? 'active' : '' }}">Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="search_box pull-right">
					<form action="{{ route('search') }}" method="get">
						<input type="text" placeholder="Search" name="key" required="required" />
						<button class="fa fa-search" type="submit"></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div><!--/header-bottom-->