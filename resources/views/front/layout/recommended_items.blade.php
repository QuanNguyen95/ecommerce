<div class="recommended_items"><!--recommended_items-->
	<h2 class="title text-center">recommended items</h2>

	<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			@php $count = 0; @endphp
			@for ($i = 0; $i < ceil(count($recommended_items)/3); $i++)
			<div @if ($i == 0) class="item active" @endif class="item">	
				@for ($j = $i*3; $j < ($i+1)*3; $j++)
				<div class="col-sm-4">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<a href="{{ route('product',[$recommended_items[$j]['id'],$recommended_items[$j]['pro_nameURL']]) }}" title="">
									<img src="{{ asset('public/image/product') }}/{{$recommended_items[$j]['pro_img']}}" alt="" />
								</a>
								@if ($recommended_items[$j]['sale'])
								<h2><span class="col-sm-6 checksale">${{$recommended_items[$j]['pro_price']}}</span> <span class="col-sm-6">${{$recommended_items[$j]['sql_price']}}</span></h2>
								@else
                            		<h2><span class="col-sm-12" >${{$recommended_items[$j]['pro_price']}}</span></h2>
                            	@endif

								<a href="{{ route('product',[$recommended_items[$j]['id'],$recommended_items[$j]['pro_nameURL']]) }}" title=""><p>{{$recommended_items[$j]['pro_name']}}</p></a>
								<a href="{{ route('add_cart', $recommended_items[$j]['id']) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
							</div>

						</div>
					</div>
				</div>
				@endfor
			</div>
			@php $count++; @endphp
			@endfor
		</div>
		<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
			<i class="fa fa-angle-left"></i>
		</a>
		<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
			<i class="fa fa-angle-right"></i>
		</a>			
	</div>
</div><!--/recommended_items-->