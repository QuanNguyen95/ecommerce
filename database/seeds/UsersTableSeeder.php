<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Quân Nguyễn',
            'email' => 'conan989hd@gmail.com',
            'password' => bcrypt('anhgahd'),
            'admin' => true, 
            'active' => true,
            'created_at' => DateTime('2018-05-29 00:00:00'),
        ]);
    }
}
