<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//front
Route::get('index',  function () {
    return redirect()->route('index');
});
// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/', 'PageController@index')->name('index');

Route::get('category/{name_cate}.html', 'PageController@category')->name('category');

Route::get('product/{id}/{pro_name}.html', 'PageController@product')->name('product');

Route::get('contact.html', 'PageController@contact')->name('contact');

Route::get('search.html', 'PageController@search')->name('search');

Route::get('login.html', 'PageController@getLogin')->name('login');
Route::post('login.html', 'PageController@postLogin')->name('login');

Route::get('logout.html', 'PageController@getLogout')->name('logout');

Route::post('signup.html', 'PageController@postSignup')->name('signup');

Route::get('user/activation/{token}.html', 'PageController@activateUser')->name('user.activate');

//cart
Route::get('cart.html', 'CartController@cart')->name('cart');

Route::get('cart/add/{id}.html', 'CartController@addCart')->name('add_cart');

Route::get('cart/addAjax.html', 'CartController@addCartAjax')->name('addAjax_cart');

Route::get('cart/update.html', 'CartController@updateCart')->name('update_cart');

Route::get('cart/remove/{rowId}.html', 'CartController@removeCart')->name('remove_cart');




// auth user
Route::group(['middleware' => 'auth'], function() {

    Route::get('profile.html', 'CustomerController@getProfile')->name('profile');

    Route::post('profile.html', 'CustomerController@postProfile')->name('profile');

    Route::get('checkout.html', 'CustomerController@getCheckout')->name('checkout');

    Route::post('checkout.html', 'CustomerController@postCheckout')->name('checkout');

    Route::get('thankyou.html', 'CustomerController@getThankyou')->name('thankyou');

    Route::get('orders.html', 'CustomerController@getOrder')->name('orders');

    Route::get('order/{id}/detail.html', 'CustomerController@getOrderDetail')->name('orderdetail');

    Route::post('review.html', 'CustomerController@review')->name('review');

});




//admin
Route::get('admin/login', 'AdminController@getLogin')->name('admin_login');

Route::post('admin/login', 'AdminController@postLogin')->name('admin_login');

Route::get('admin/logout', 'AdminController@getLogout')->name('admin_logout');

// Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function() {
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
    Route::get('/', 'AdminController@index')->name('index_admin');
    Route::get('index', function () {
        return redirect()->route('index_admin');
    });

	Route::prefix('category')->group(function () {
        
        Route::get('list', 'AdminController@getCateList')->name('category_list');

        Route::get('edit/{id}', 'AdminController@getCateEdit');
        Route::post('edit/{id}', 'AdminController@postCateEdit');

        Route::get('add', 'AdminController@getCateAdd')->name('category_add');
        Route::post('add', 'AdminController@postCateAdd')->name('category_add');

        Route::get('delete/{id}', 'AdminController@getCateDelete')->name('category_delete');
    });

    Route::prefix('product')->group(function () {
        
        Route::get('list', 'AdminController@getProductList')->name('product_list');

        Route::get('edit/{id}', 'AdminController@getProductEdit')->name('product_edit');
        Route::post('edit/{id}', 'AdminController@postProductEdit')->name('product_edit');

        Route::get('add', 'AdminController@getProductAdd')->name('product_add');
        Route::post('add', 'AdminController@postProductAdd')->name('product_add');

        Route::get('delete/{id}', 'AdminController@getProductDelete')->name('product_delete');

        Route::get('addSale', 'AdminController@addSale')->name('addSale');
        Route::get('editSale', 'AdminController@editSale')->name('editSale');

        Route::get('addAlt/{id}', 'AdminController@getAltAdd')->name('addAlt'); 
        Route::post('addAlt/{id}', 'AdminController@postAltAdd')->name('addAlt'); 

        Route::get('{id_pro}/deleteAlt/{id_alt}', 'AdminController@deleteAlt')->name('deleteAlt'); 
    });

    Route::prefix('users')->group(function () {
        Route::get('list', 'AdminController@getUsersList')->name('users_list');

        Route::get('edit/{id}', 'AdminController@getUsersEdit')->name('users_edit');
        Route::post('edit/{id}', 'AdminController@postUsersEdit')->name('users_edit');
        
        Route::get('delete/{id}', 'AdminController@getUsersDelete')->name('users_delete');
    });
});

Route::get('put', function() {
    Storage::cloud()->put('test.txt', 'Hello World');
    return 'File was saved to Google Drive';
}); 


// Route::any('{all?}',function () {
//     return redirect()->route('index');
// })->where('all', '(.*)');


