<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_signup' => 'required',
            'email_signup' => 'email|required|unique:users,email',
            'password_signup' => 'required|min:6',
            'confirmpassword_signup' => 'required|same:password_signup',
        ];
    }

    public function messages()
    {
        return [
            'name_signup.required' => 'Name is required',
            'email_signup.required' => 'Email address is required',
            'email_signup.email' => 'Email invalid',
            'email_signup.unique' => 'Email already exist',
            'password_signup.required' => 'Password is required',
            'password_signup.min' => 'Passwords are at least 6 characters long',
            'confirmpassword_signup.required' => 'Password confirm is required',
            'confirmpassword_signup.same' => 'The confirm password  is incorrect',
        ];
    }
}
