<?php

namespace App\Http\Controllers;

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function cart()
    {
    	$cartItems = Cart::content();
        return view('front.pages.cart', compact('cartItems'));
    }

    public function addCart(Request $request)
    {
        $id = $request->id;

        $product = Product::find($id);
        $qty = 1;
        if($product->sale) {
            $price = $product->sql_price;
        } else {
            $price = $product->pro_price;
        }
        Cart::add(array('id'=>$id, 'name'=>$product->pro_name, 'price'=>$price, 'qty' => $qty, 'options' => ['image'=>$product->pro_img , 'code'=>$product->pro_code, 'nameURL'=>$product->pro_nameURL]));
        return redirect()->back();
    }

    public function addCartAjax(Request $request)
    {
    	if($request->ajax()){
    		$pro_id =  $request->pro_id;
    		$qty = $request->qty;

    		$product = Product::find($pro_id);
    		if($product->sale) {
	            $price = $product->sql_price;
	        } else {
	            $price = $product->pro_price;
	        }

	        Cart::add(array('id'=>$pro_id, 'name'=>$product->pro_name, 'price'=>$price, 'qty' => $qty, 'options' => ['image'=>$product->pro_img , 'code'=>$product->pro_code, 'nameURL'=>$product->pro_nameURL]));
	        return json_encode(array("status"=>"TRUE","totalProductCart"=> Cart::count()));
    	}
    }

    public function updateCart(Request $request)
    {
        if($request->ajax()){
            $rowId =  $request->rowId;
            $qty = $request->qty;
            if($qty > 0)
            {
                Cart::update($rowId, $qty);

                $item = Cart::get($rowId);
                return json_encode(array("status"=>"TRUE"));
            }
            else
            {
                return json_encode(array("status"=>"FALSE"));
            } 
        }
    }

    public function removeCart(Request $request)
    {
        $rowId = $request->rowId;
        Cart::remove($rowId);
        return redirect()->back();
    }


}
