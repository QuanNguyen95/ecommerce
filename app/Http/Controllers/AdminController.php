<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\LoginRequest;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    // middleware
    public function getLogin()
    {
        if(!Auth::check()) {
            return view('admin.login');
        } else {
            return redirect()->route('index_admin');
        }
    }

    public function postLogin(LoginRequest $request)
    {
        $data = array('email' => $request->email, 'password'=>$request->password);
        if(Auth::attempt($data)) {
            return redirect()->route('index_admin');
        }
        else {
            session()->flash('message', 'Login failed!');
            return redirect()->back();
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('admin_login');
    }

    public function index()
    {
        return view('admin.pages.index');
    }

    //Category
    public function getCateList()
    {
    	$category = Category::all();
    	return view('admin.pages.category.list', compact('category'));
    }

	public function getCateEdit(Request $request)
    {
    	$cate_id = $request->id;
    	$category = Category::find($cate_id);
    	return view('admin.pages.category.edit', ['category' => $category]);
    }

    public function postCateEdit(Request $request, $id)
    {
    	$category = Category::find($id);

    	$this->validate($request,
    		[
    			'cat_name' => 'required|unique:category,name'
    		],
    		[
    			'cat_name.required' => 'Category name is require',
    			'cat_name.unique' => 'Category name is exist',
    		]
    	);

    	$category->name = $request->input('cat_name');
        $category->nameURL = changeTitle($request->input('cat_name'));

    	$category->save();
    	$request->session()->flash('message', 'Edit category success!');
    	return redirect('admin/category/list');
    }

    public function getCateDelete($id)
    {
    	DB::table('category')->where('id' , $id)->delete();
    	return redirect()->back()->with('message','Delete category success!');
    }

    public function getCateAdd()
    {
    	return view('admin.pages.category.add');
    }
    
    public function postCateAdd(Request $request)
    {

    	$this->validate($request,
    		[
    			'cat_name' => 'required|unique:category,name'
    		],
    		[
    			'cat_name.required' => 'Category name is require',
    			'cat_name.unique' => 'Category name is exist',
    		]
    	);

    	$category = new Category;
    	$category->name = $request->input('cat_name');
        $category->nameURL = changeTitle($request->input('cat_name'));

    	$category->save();
    	$request->session()->flash('message', 'Add category success!');
    	return redirect()->route('category_list');
    }
    
    //Product
    public function getProductList()
    {
    	$product = Product::orderBy('id', 'desc')->paginate(10);
    	return view('admin.pages.product.list', compact('product'));
    }

    public function getProductEdit($id)
    {
        $category = Category::all();
    	$product = Product::find($id);
        return view('admin.pages.product.edit', compact('product','category'));
    }

    public function postProductEdit(Request $request, $id)
    {
        $product = Product::findOrFail($id);
    	$this->validate($request,
            [
                'cat_id' => 'required',
                'pro_name' => 'required',
                'pro_price' => 'required|numeric|min:0',
                'pro_info' => 'required',
                'pro_img' => 'image',
                'sql_price' => 'min:0'
            ],
            [
                'cat_id.required' => 'The category is require',
                'pro_name.required' => 'The name product is required',
                'pro_price.required' => 'The price product is required',
                'pro_price.numeric' => 'The price product must be a number',
                'pro_price.min' => 'The price product min is 0',
                'pro_info.required' => 'The info product is required',
                'pro_img.image' => 'The file must be an image',
                'sql_price.min' => 'The price sale min is 0',
            ]
        );

        
        $product->id_cate = $request->cat_id;
        $product->pro_name = $request->pro_name;
        $product->pro_nameURL = changeTitle($request->input('pro_name'));
        $product->pro_price = $request->pro_price;
        $product->pro_code = $request->pro_code;
        $product->pro_info = $request->pro_info;
        $product->sale = $request->sale;

        if($request->has('sql_price')) {
            $product->sql_price = $request->sql_price;
        }

        if ($request->hasFile('pro_img')) 
        {
            if(File::exists("public/image/product/".$product->pro_img)){
                File::delete("public/image/product/".$product->pro_img);
            }

            $file = $request->file('pro_img');
            $name = time().$file->getClientOriginalName();  

            $path = "public/image/product";
            $file->move($path,$name);

            $product->pro_img = $name;
        }

        $product->save();

        return redirect()->back()->with('message','Edit product success!');
    }

    public function getProductAdd()
    {
        $category = Category::all();
    	return view('admin.pages.product.add', ['category' => $category]);
    }

    public function postProductAdd(Request $request)
    {
        $this->validate($request,
            [
                'cat_id' => 'required',
                'pro_name' => 'required',
                'pro_price' => 'required|numeric|min:0',
                'pro_code' => 'required|unique:product,pro_code',
                'pro_info' => 'required',
                'pro_img' => 'required|image',
                'sql_price' => 'min:0'
            ],
            [
                'cat_id.required' => 'The category is require',
                'pro_name.required' => 'The name product is required',
                'pro_price.required' => 'The price product is required',
                'pro_price.numeric' => 'The price product must be a number',
                'pro_price.min' => 'The price product min is 0',
                'pro_code.required' => 'The code product is required',
                'pro_code.unique' => 'The code product is exist',
                'pro_info.required' => 'The info product is required',
                'pro_img.required' => 'The image product is required',
                'pro_img.image' => 'The file must be an image',
                'sql_price.min' => 'The price sale min is 0',
            ]
        );

        if ($request->hasFile('pro_img')) 
        {
            $file = $request->file('pro_img');
            $name = time().$file->getClientOriginalName();  

            $path = "public/image/product";
            $file->move($path,$name);

            // $product->pro_img = $name;
        }

        $product = new Product;
        $data = [
            'pro_name' => $request->pro_name,
            'pro_nameURL' => changeTitle($request->input('pro_name')),
            'pro_code' => $request->pro_code,
            'pro_price' => $request->pro_price,
            'pro_info' => $request->pro_info,
            'pro_img' => $name,
            'sql_price' => $request->sql_price,
            'id_cate' => $request->cat_id,
            'sale' => $request->sale,
        ];

        $product->fill($data);
        // $product->id_cate = $request->cat_id;
        // $product->pro_name = $request->pro_name;
        // $product->pro_nameURL = changeTitle($request->input('pro_name'));
        // $product->pro_price = $request->pro_price;
        // $product->pro_code = $request->pro_code;
        // $product->pro_info = $request->pro_info;
        // $product->sale = $request->sale;

        // if($request->has('sql_price')) {
        //     $product->sql_price = $request->sql_price;
        // }

        $product->save();

        if ($request->hasFile('alt_images')) 
        {
            $request->validate(
            [
                'alt_images.*' => 'image'
            ],
            [
                'alt_images.*.image' => 'The file alt image must be an image' 
            ]);

            $file = $request->file('alt_images');

            foreach ($file as $value) {
                $name = time().$value->getClientOriginalName();  

                $path = "public/image/alt_images";
                $value->move($path,$name);

                $add_alt = DB::table('alt_images')->insert(['id_product' => $product->id, 'alt_img' => $name]);
            }
        }

        return redirect()->back()->with('message','Add product success!');
    }

    public function getProductDelete($id)
    {
    	$pro = Product::findOrFail($id);

        if(File::exists("public/image/product/".$pro->pro_img)){
            File::delete("public/image/product/".$pro->pro_img);
        }

        $alt = DB::table('alt_images')->where('id_product', $id)->get();

        foreach ($alt as $value) {
            if(File::exists("public/image/alt_images/".$value->alt_img)){
                File::delete("public/image/alt_images/".$value->alt_img);
            }
        }

        $pro->delete();

        return redirect()->back()->with('message','Delete product success!');
    }

    public function addSale(Request $request)
    {
        $salePrice = $request->salePrice;
        $pro_id = $request->pro_id;
        DB::table('product')->where('id', $pro_id)->update(['sql_price' => $salePrice]);
        echo 'Added successfully';
    }

    public function editSale(Request $request)
    {
        $pro_id = $request->pro_id;
        $sale = $request->sale;
        DB::table('product')->where('id', $pro_id)->update(['sale' => $sale]);
        echo "successfully";
    }

    public function getAltAdd($id)
    {
        $proInfo = DB::table('product')->where('id', $id)->first();
        return view('admin.pages.product.altImage', compact('proInfo'));
    }

    public function postAltAdd(Request $request, $id)
    {
        $this->validate($request,
            [
                'image' => 'image',
            ],
            [
                'image.image' => 'The file is not an image',
            ]
        );

        if ($request->hasFile('image')) 
        {
            $file = $request->file('image');
            $name = time().$file->getClientOriginalName();  

            $path = "public/image/alt_images";
            $file->move($path,$name);
            $add_alt = DB::table('alt_images')->insert(['id_product' => $id, 'alt_img' => $name]);
            return redirect()->back()->with('message','Add Alt image success!');
        }
        return redirect()->back();
    }

    public function deleteAlt($id_pro , $id_alt)
    {
        $alt = DB::table('alt_images')->where('id', $id_alt)->first();
        if(File::exists("public/image/alt_images/".$alt->alt_img)){
            File::delete("public/image/alt_images/".$alt->alt_img);
        }
        $alt_delete = DB::table('alt_images')->where('id', $id_alt)->delete();
        return redirect()->back()->with('message','Delete Alt image success!');
    }

    //Users
    public function getUsersList()
    {
        $users = User::all();
        return view('admin.pages.user.list', compact('users'));
    }

    public function getUsersEdit($id)
    {
        $user = User::find($id);
        return view('admin.pages.user.edit', compact('user'));
    }

    public function postUsersEdit(Request $request, $id)
    {
        $user = User::find($id);
        $request->validate(
        [
            'name' => 'required',
        ],
        [
            'name.required' => 'User name is required',
        ]);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if ($request->has('active')) {
            $user->active = $request->input('active');
        }
        
        if($request->has('changePassword'))
        {
            $request->validate(
            [
                'password' => 'required|min:6',
                'confirmPassword' => 'required|same:password',
            ],
            [
                'password.required' => 'Password is required',
                'password.min' => 'Password are at least 6 characters long',
                'confirmPassword.required' => 'Confirm password is required',
                'confirmPassword.same' => 'The confirm password is incorrect',
            ]);
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();
        
        return redirect()->back()->with('message','Edit user success!');
    }

    public function getUsersDelete($id)
    {
        $user = User::find($id);
        if ($user->admin) {
            return redirect()->back()->with('error','Cannot delete this user!');
        }
        else {
            $user->delete();
            return redirect()->back()->with('message','Delete user success!');
        }
        
    }

    
}
