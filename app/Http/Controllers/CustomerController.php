<?php

namespace App\Http\Controllers;

use App\Mail\OrderShipped;
use App\Order;
use App\OrderDetails;
use App\OrderInfo;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{

    public function getProfile()
    {
    	return view('front.pages.profile');
    }

    public function postProfile(Request $request)
    {
    	$id = $request->id_user;
    	$user = User::find($id);
        $request->validate(
        [
            'name' => 'required',
        ],
        [
            'name.required' => 'User name is required',
        ]);

        $user->name = $request->input('name');
        
        if($request->has('changePassword'))
        {
            $request->validate(
            [
                'password' => 'required|min:6',
                'confirmPassword' => 'required|same:password',
            ],
            [
                'password.required' => 'Password is required',
                'password.min' => 'Password are at least 6 characters long',
                'confirmPassword.required' => 'Confirm password is required',
                'confirmPassword.same' => 'The confirm password is incorrect',
            ]);
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();
        
        return redirect()->back()->with('message','Successful information change!');
    }

    public function getCheckout()
    {
        $cartItems = Cart::content(); 
        return view('front.pages.checkout', compact('cartItems'));
    }

    public function postCheckout(Request $request)
    {   
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required',
        ],
        [
            'name.required' => 'Please enter name',
            'email.required' => 'Please enter email',
            'phone.required' => 'Please enter phone',
            'address.required' => 'Please enter address',
        ]);

        $id_user = Auth::user()->id;

        $order = new Order;
        $order->id_user = $id_user;
        $order->total = Cart::total();
        $order->payment = $request->pay;
        $order->status = "pending";
        $order->save();

        foreach (Cart::content() as $cartItem) {
            $order_details = new OrderDetails;
            $order_details->id_order = $order->id;
            $order_details->id_product = $cartItem->id;
            $order_details->quantity = $cartItem->qty;
            $order_details->price = $cartItem->price;
            $order_details->tax = $cartItem->tax();
            $order_details->save();
        }

        $order_info = new OrderInfo;
        $order_info->id_order = $order->id;
        $order_info->id_user = $id_user;
        $order_info->name = $request->name;
        $order_info->email = $request->email;
        $order_info->phone = $request->phone;
        $order_info->address = $request->address;
        $order_info->note = $request->message;
        $order_info->save();

        Mail::to($request->email)->send(new OrderShipped(new Cart));

        Cart::destroy();

        return redirect()->route('thankyou');

    }

    public function getThankyou()
    {
        return view('front.pages.thankyou');
    }

    public function getOrder()
    {
        $orders = Order::where('id_user', Auth::id())->get();
        return view('front.pages.order', compact('orders'));
    }
    
    public function getOrderDetail($id)
    {
        $order = Order::find($id);
        if($order == null || $order->id_user != Auth::id()) {
            return view('front.pages.error');
        }
        $order_details = OrderDetails::where('id_order',$id)->get();
        return view('front.pages.orderdetail', compact(['order', 'order_details']));
    }

    public function review(Request $request)
    {
        $id = Auth::id();
        $rating = $request->rating;
        $comment = $request->comment;
        echo $rating;
        echo "<br>";
        echo $comment;
    }
}
