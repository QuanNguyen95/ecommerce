<?php

namespace App\Http\Controllers;

use App\Category;
use App\Classes\ActivationService;
use App\Comment;
use App\Http\Requests\SignupRequest;
use App\Product;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    protected $activationService;

    public function __construct(ActivationService $activationService)
    {
        $this->activationService = $activationService;
    }

    public function index()
    {
        $products = Product::orderBy('id','desc')->paginate(6);
        return view('front.pages.index', compact('products'));
    }

    public function category(Request $request)
    {
        $name = $request->name_cate;
        $cate = Category::where('nameURL', $name)->first();
        if($cate == null) {
            return view('front.pages.error');
        }
        $products = DB::table('product')->where('id_cate', $cate->id)->paginate(6);
        
        return view('front.pages.product', compact('products'));
    }

    public function product(Request $request)
    {
        $id = $request->id;
        $pro = Product::find($id);
        
        if(empty($pro)) {
            return view('front.pages.error');
        }
        $altImgs = DB::table('alt_images')->where('id_product', $pro->id)->get(); 

        $comments = Comment::where('id_product', $id)->orderBy('created_at', 'asc')->get();
        
        return view('front.pages.product-details', compact(['pro','altImgs','comments']));
    }

    public function contact()
    {
        return view('front.pages.contact');
    }

    public function getLogin()
    {
        if(!Auth::check()) {
            return view('front.pages.login');
        } else {
            return redirect()->route('index');
        }
        
    }

    public function postLogin(Request $request)
    {
        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password])) 
        {
            if(!Auth::user()->active)
            {
                $this->activationService->sendActivationMail(Auth::user());
                Auth::logout();
                return redirect()->back()->with('message', 'Login failed! You need to validate your account, we have sent the verification code to your email, please check and follow the instructions!');
            }else {
                return redirect()->back()->with('message', 'Logged in successfully!');
            }
        } 
        else
        {
            return redirect()->back()->with('message', 'Login failed!');
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function postSignup(SignupRequest $request)
    {
        $token = $request->get('g-recaptcha-response');

        if ($token) {
            $client = new Client();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => array(
                    'secret'    => '6Ldf9VwUAAAAAEoWY_UOwYor_F65AG59Lb_VdEMB',
                    'response'  => $token
                )
            ]);
            $results = json_decode($response->getBody()->getContents());
            if ($results->success) {
                $user = new User();
                $user->name = $request->input('name_signup');
                $user->email = $request->input('email_signup');
                $user->password = Hash::make($request->password_signup);
                $user->save();

                $this->activationService->sendActivationMail($user);

                return redirect()->back()->with('status', 'Signup successfully. Please check email and execute the manual.');
            } else {
                Session::flash('error', 'You are probably a robot!');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Please check Captcha!');
            return redirect()->back();
        }

        

    }

    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            auth()->login($user);
            return redirect('/login');
        }
        return view('front.pages.error');
    }

    public function search(Request $request)
    {
        $key = $request->get('key');
        $products = Product::where('pro_name', 'like', '%'.$key.'%')->paginate(6);
        return view('front.pages.product', compact(['products','key']));
    }

    
}
