<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            if($user->admin == 1)
                return $next($request);
            else{
                Auth::logout();
                return redirect()->route('admin_login')->with('message', 'Login failed!');
            }
        }
        else
        {
            return redirect()->route('admin_login');
        }
    }
}
