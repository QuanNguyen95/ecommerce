<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    protected $fillable = ['pro_name','pro_nameURL','pro_code','pro_price','pro_info','pro_img','sql_price','id_cate','sale'];

    public function categories()
    {
    	return $this->belongsTo('App\Category','id_cate','id');
    }
}
