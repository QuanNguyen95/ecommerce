<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderInfo extends Model
{
    protected $table = 'order_info';

    public function order()
    {
    	return $this->hasOne('App\Order', 'id_order', 'id');
    }
    
}
