<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    public function order_details()
    {
    	return $this->hasMany('App\OrderDetails','id_order','id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User','id_user','id');
    }

    public function orderinfo()
    {
    	return $this->hasOne('App\OrderInfo', 'id_order', 'id');
    }

}
