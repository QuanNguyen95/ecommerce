<?php

namespace App\Providers;

use App\Category;
use App\Product;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['front.layout.left-sidebar','front.layout.menu', 'front.pages.index'], function ($view) {
            $category = Category::all();
            $view->with('category',$category);
        });
        View::composer(['front.layout.recommended_items'], function ($view) {
            $recommended_items = Product::where('created_at', '>', Carbon::now()->subDay(7))->orWhere('sale',true)->distinct()->inRandomOrder()->take(6)->get()->toArray();
            $view->with('recommended_items',$recommended_items);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
